Conferma Connector
==================

An XML webservice that operates over CLI to obtain card numbers from the Conferma SOAP API.

## Build

```
docker build -t apache-ant .
docker run --rm -v "$PWD":/app -w /app apache-ant ant
```

## Distribution

Copy files from `dist/`
